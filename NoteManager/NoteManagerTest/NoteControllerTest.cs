﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using NoteManager.Models;
using NoteManager.Controllers;
using NoteManager.Logic;
using System.Collections.Generic;
using System.Linq;
namespace NoteManagerTest
{
    [TestClass]
    public class NoteControllerTest
    {
        [TestMethod]
        public void TestDetailsView()
        {
            var controller = new NotesController();

            ViewResult result = controller.Index("","") as ViewResult;

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void TestExtensionMethod()
        {
            IEnumerable<Note> notes = new List<Note>
            {
                 new Note {NoteID=1,Category=Category.Sport,Priority=Priority.High,Text="sit",Title="GYM" },
                 new Note {NoteID=2,Category=Category.Study,Priority=Priority.Low,Text="laba 2",Title="DO" },
                 new Note {NoteID=3,Category=Category.Study,Priority=Priority.Middle,Text="test unit",Title="Unit" },
                 new Note {NoteID=4,Category=Category.Sport,Priority=Priority.High,Text="bench press",Title="GYM" },
            };

            var real = notes.IsEmpty();
            notes = notes.Where(s => s.Category == Category.Work);

            Assert.AreEqual(real,true);
        }
        [TestMethod]
        public void Contact()
        {
            var controller = new NotesController();

            ViewResult result = controller.Create() as ViewResult;

            Assert.IsNotNull(result);
        }
        
    }
}
