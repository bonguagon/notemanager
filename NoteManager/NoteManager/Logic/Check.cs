﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoteManager.Models;
namespace NoteManager.Logic
{
    public class Helper
    {
        public static IQueryable<Note> Search(string searchString, IQueryable<Note> notes)
        {
            if (!String.IsNullOrEmpty(searchString))
            {
               notes = notes.Where(s => s.Title.Contains(searchString));
            }
            return notes;
        }
    }
}