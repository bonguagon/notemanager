﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoteManager.Logic
{
    public static class Utils
    {
        public static bool IsEmpty<T>(this IEnumerable<T> data)
        {
            return data != null && data.Any();
        }
    }
}