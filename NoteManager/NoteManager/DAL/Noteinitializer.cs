﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoteManager.Models;
namespace NoteManager.DAL
{
    public class Noteinitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<NoteContext>
    {

        protected override void Seed(NoteContext context)
        {
            var notes = new List<Note>
            {
                new Note{NoteID=1,Category=Category.Study,Title="DO",Text="Make lab 3",Priority=Priority.Middle},
            };
            notes.ForEach(s => context.Notes.Add(s));
            context.SaveChanges();
        }
       
       
    }
}