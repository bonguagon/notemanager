﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using NoteManager.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NoteManager.DAL
{
    public class NoteContext:DbContext
    {
        public NoteContext() : base("NoteContext")
        {
        }

        public DbSet<Note> Notes { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}