﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NoteManager.DAL;
using NoteManager.Models;
using NoteManager.Logic;
namespace NoteManager.Controllers
{
    public class NotesController : Controller
    {
        private NoteContext db = new NoteContext();

        // GET: Notes
        public ActionResult Index(string sortOrder,string SearchString)
        {
            ViewBag.PrioritySortParam = String.IsNullOrEmpty(sortOrder) ? "Priority" : "";
            ViewBag.PrioritySortParam = sortOrder=="Priority_desc" ? "Priority" : "Priority_desc";
            try
            {
                var notes = from s in db.Notes
                            select s;
                if (!String.IsNullOrWhiteSpace(SearchString))
                {
                    notes = Helper.Search(SearchString, notes);
                    if (notes.IsEmpty())
                    {
                        return View(notes.ToList());
                    }
                }
                switch (sortOrder)
                {
                    case "Priority":
                        notes = notes.OrderBy(s => s.Priority.Value);
                        break;
                    case "Priority_desc":
                        notes = notes.OrderByDescending(s => s.Priority.Value);
                        break;
                    default:
                        break;
                }
                return View(notes.ToList());
            }
            catch (Exception e) { }
            return View();
        }

        // GET: Notes/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.message = "Test message";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // GET: Notes/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NoteID,Title,Category,Text,Priority")] Note note)
        {
            if (ModelState.IsValid)
            {
                db.Notes.Add(note);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(note);
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NoteID,Title,Category,Text,Priority")] Note note)
        {
            if (ModelState.IsValid)
            {
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Note note = db.Notes.Find(id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
