﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace NoteManager.Models
{
    public  enum Category { Study, Work, Sport }
    public enum Priority { High=3,Middle=2,Low=1}
    public class Note
    {
        public int NoteID { get; set; }
        public String Title { get; set; }
        public Category? Category { get; set; }
        public Priority? Priority { get; set; }
        public string Text { get; set; }
    }
}